package com.tsyklop.towns.game;

import com.tsyklop.towns.game.exception.TownIncorrectException;
import com.tsyklop.towns.game.exception.TownNotFoundException;
import org.apache.commons.io.IOUtils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.*;

import static com.tsyklop.towns.game.Utils.getFirstChar;

public class Game {

    private AI ai;

    public static final String TOWN_NOT_FOUND_MESSAGE = "Такой город не существует. Я всеее знаю)";

    public Game() {
        this.ai = new AI(loadTowns());
    }

    public void start() {

        System.out.println();

        Scanner scanner = new Scanner(System.in);

        System.out.println("Начинай первый, вводи город!");

        while (true) {
            try {
                ai.processTown(scanner.nextLine());
            } catch (TownNotFoundException | TownIncorrectException e) {
                System.err.println(e.getMessage());
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

    }

    private Map<Character, List<String>> loadTowns() {

        Map<Character, List<String>> townsMap = new HashMap<>();

        try (InputStream is = Game.class.getClassLoader().getResourceAsStream("towns.txt")) {

            if (is == null) {
                throw new FileNotFoundException("towns.txt not found in resources");
            }

            IOUtils.readLines(is, StandardCharsets.UTF_8).forEach(town -> {
                if (town != null && !town.isEmpty()) {

                    town = town.trim();

                    Character firstChar = getFirstChar(town);

                    if (townsMap.containsKey(firstChar)) {
                        townsMap.get(firstChar).add(town);
                    } else {
                        townsMap.put(firstChar, new ArrayList<>(Collections.singletonList(town)));
                    }

                }
            });

        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return townsMap;

    }

}
