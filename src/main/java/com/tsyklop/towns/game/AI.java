package com.tsyklop.towns.game;

import com.tsyklop.towns.game.exception.TownIncorrectException;
import com.tsyklop.towns.game.exception.TownNotFoundException;
import org.apache.commons.lang3.RandomUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.tsyklop.towns.game.Game.TOWN_NOT_FOUND_MESSAGE;
import static com.tsyklop.towns.game.Utils.getFirstChar;
import static com.tsyklop.towns.game.Utils.getLastChar;

public class AI {

    private String aiLastRespondedTown;

    private final Map<Character, List<String>> towns;

    private final List<String> aiRespondedTowns = new ArrayList<>();
    private final List<String> userRespondedTowns = new ArrayList<>();

    private static List<Character> charactersBlackList = new ArrayList<>();

    static {
        charactersBlackList.add('ь');
        charactersBlackList.add('ъ');
        charactersBlackList.add('ы');
        charactersBlackList.add('ё');
    }

    public AI(Map<Character, List<String>> towns) {
        this.towns = towns;
        this.aiLastRespondedTown = null;
    }

    public void processTown(String town) {

        checkEnteredTown(town);

        checkIfEnteredTownExist(town);

        if (this.aiRespondedTowns.contains(town)) {
            throw new TownIncorrectException("Я уже называл этот город.");
        }

        if (this.userRespondedTowns.contains(town)) {
            throw new TownIncorrectException("Ты уже называл этот город.");
        }

        if (this.aiLastRespondedTown != null) {
            Character respondedTownLastChar = getLastChar(this.aiLastRespondedTown);
            if (respondedTownLastChar != null && !respondedTownLastChar.equals(getFirstChar(town))) {
                throw new TownIncorrectException("Думал схитрить? Тебе город на букву '" + respondedTownLastChar + "'");
            }
        }

        this.userRespondedTowns.add(town);

        int charIndex = town.length() - 1;

        while (true) {

            if (charIndex < 0) {
                throw new TownNotFoundException("Я не нашел города ни по одному символу :(");
            }

            Character lastChar = getLastChar(town, charIndex);

            //checkIfLastCharIsAvailable(lastChar);

            if (charactersBlackList.contains(lastChar)) {
                charIndex--;
            } else if (towns.containsKey(lastChar)) {

                List<String> charTowns = towns.get(lastChar);

                if (charTowns == null || charTowns.isEmpty()) {
                    throw new TownNotFoundException("Твоя взяла! Я не знаю городов на букву '" + lastChar + "'");
                }

                String respondTown = charTowns.remove(RandomUtils.nextInt(0, charTowns.size()));

                if (respondTown != null) {

                    this.aiLastRespondedTown = respondTown;

                    this.aiRespondedTowns.add(respondTown);

                    System.out.println("О! Вспомнил город! Держи: " + respondTown);
                    System.out.println("Тебе город на букву '" + getLastChar(respondTown) + "'");

                    break;

                } else {
                    charIndex--;
                }

            } else {
                charIndex--;
                //throw new TownNotFoundException("Я не знаю городов начинающихся с буквы '" + lastChar + "'.");
            }

        }

    }

    private void checkEnteredTown(String town) {
        if (town == null || town.isEmpty() || !town.matches("^[А-Яа-я].*$")) {
            throw new TownIncorrectException("Ввод некорретный. Повторите пожалуйста");
        }
    }

    private void checkIfEnteredTownExist(String town) {
        Character firstChar = getFirstChar(town);
        if (town == null || !towns.containsKey(firstChar) || !towns.get(firstChar).contains(town)) {
            throw new TownNotFoundException(TOWN_NOT_FOUND_MESSAGE);
        }
    }

    private void checkIfLastCharIsAvailable(Character lastChar) {
        if (lastChar == null) {
            throw new TownIncorrectException("Ошибка: Последний символ не найден");
        }
    }

}
