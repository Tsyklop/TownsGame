package com.tsyklop.towns.game.exception;

public class TownIncorrectException extends RuntimeException {

    public TownIncorrectException() {
    }

    public TownIncorrectException(String message) {
        super(message);
    }

    public TownIncorrectException(String message, Throwable cause) {
        super(message, cause);
    }

    public TownIncorrectException(Throwable cause) {
        super(cause);
    }

    public TownIncorrectException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
