package com.tsyklop.towns.game.exception;

public class TownNotFoundException extends RuntimeException {

    public TownNotFoundException() {
    }

    public TownNotFoundException(String message) {
        super(message);
    }

    public TownNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public TownNotFoundException(Throwable cause) {
        super(cause);
    }

    public TownNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
