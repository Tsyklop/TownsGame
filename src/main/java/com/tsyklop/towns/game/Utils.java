package com.tsyklop.towns.game;

public class Utils {

    public static Character getLastChar(String str) {
        try {
            return str != null ? Character.toLowerCase(str.charAt(str.length() - 1)) : null;
        } catch (StringIndexOutOfBoundsException e) {
            return null;
        }
    }

    public static Character getLastChar(String str, int pos) {
        try {
            return str != null ? Character.toLowerCase(str.charAt(pos)) : null;
        } catch (StringIndexOutOfBoundsException e) {
            return null;
        }
    }

    public static Character getFirstChar(String str) {
        try {
            return str != null ? Character.toLowerCase(str.charAt(0)) : null;
        } catch (StringIndexOutOfBoundsException e) {
            return null;
        }
    }

}
